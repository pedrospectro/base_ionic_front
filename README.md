Intall cordova and ionic:
>npm install -g ionic cordova

Start App:
>ionic serve

Build app for IOS 
>ionic platform add ios

>cordova build --release ios

Build app for Android
>ionic platform add android

>cordova build --release android

Read the documentation below to know how to Deploy in apple store or google store:
http://ionicframework.com/docs/guide/publishing.html