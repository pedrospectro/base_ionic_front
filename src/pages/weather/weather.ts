import { Component } from '@angular/core';
import {ApiService} from '../../app/services/api.service';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'weather',
  templateUrl: 'weather.html'
})
export class WeatherPage {
  weather:any;
  searchStr:string;
  results:any;
  zmw:any;

  constructor(public navCtrl: NavController,private apiService:ApiService) {
  }

  getDefaultLocation(){
    
  }

  ngOnInit(){
    this.getDefaultLocation();
  }

  getQuery(){
  }

  chooseLocation(location){
    this.results=[];
   
  }
}